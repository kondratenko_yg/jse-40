package ru.kondratenko.tm;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.observer.Listener.ListenerProjectImpl;
import ru.kondratenko.tm.observer.Listener.ListenerTaskImpl;
import ru.kondratenko.tm.observer.Listener.ListenerUserImpl;
import ru.kondratenko.tm.observer.Publisher.PublisherImpl;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.service.TaskService;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.Scanner;

public class Application {
    public static final Logger logger = LogManager.getLogger(Application.class);

    static {
        UserService.getInstance().create("admin", "123", "name1", "ivnov", Role.ADMIN);
        UserService.getInstance().create("test", "123", "name2", "petrov", Role.USER);
        ProjectService.getInstance().create("Cproject1", "", UserService.getInstance().findByName("admin").get().getId());
        ProjectService.getInstance().create("Broject2", "", UserService.getInstance().findByName("admin").get().getId());
        ProjectService.getInstance().create("Aroject2", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task1", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task1", "1fg", UserService.getInstance().findByName("test").get().getId());
        TaskService.getInstance().create("task1", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task2", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("task1", "", UserService.getInstance().findByName("admin").get().getId());
        TaskService.getInstance().create("Xtask2", "", UserService.getInstance().findByName("admin").get().getId());
    }

    public static void main(final String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        var publisher = new PublisherImpl(UserService.getInstance());
        var listenerUser = new ListenerUserImpl();
        var listenerProject = new ListenerProjectImpl();
        var listenerTask = new ListenerTaskImpl();
        publisher.addListener(listenerUser);
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        try {
            publisher.start(new Scanner(System.in));
        } catch (NotFoundException | IOException exception) {
            logger.error(exception.getMessage());
        }
    }

}
