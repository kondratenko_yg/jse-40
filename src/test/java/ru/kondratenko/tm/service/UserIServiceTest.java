package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.repository.UserRepository;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.USERS_FILE_NAME_JSON;
import static ru.kondratenko.tm.constant.TerminalConst.USERS_FILE_NAME_XML;
import static ru.kondratenko.tm.util.HashUtil.hashMD5;

class UserIServiceTest {
    private UserService userService;
    private UserRepository userRepository;
    private final User user =  new User("user1",hashMD5("123"), Role.ADMIN);

    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        userService = UserService.getInstance();
        ReflectionTestUtils.setField(userService,"userRepository",userRepository);
    }

    @Test
    void create() {
        assertEquals(Optional.empty(),userService.create(null,null,null,null,null));
        assertEquals(Optional.empty(),userService.create("",null,null,null,null));
        assertEquals(Optional.empty(),userService.create("login",null,null,null,null));
        assertEquals(Optional.empty(),userService.create("login","123",null,null,null));
        assertEquals(Optional.empty(),userService.create("login","123","Ivan",null,null));
        assertEquals(Optional.empty(),userService.create("login","123","Ivan","Ivanov",null));
        when(userRepository.create(Mockito.any()))
                .thenReturn(Optional.of(user));
        assertEquals("user1",
                userService.create("user1","123","Ivan","Ivanov", Role.ADMIN).get().getName());
        userService.clear();
    }
    @Test
    void checkPassword() {
        assertTrue(userService.checkPassword(Optional.of(user),"123"));
    }

    @Test
    void findByLogin()  {
        assertEquals(Optional.empty(),userService.findByName(null));
        assertEquals(Optional.empty(),userService.findByName(""));
        when(userRepository.findByName("user1")).thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.findByName("user1"));
    }

    @Test
    void findById()  {
        assertEquals(Optional.empty(),userService.findById(null));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.findById(1L));
    }

    @Test
    void updateByLogin() {
        assertEquals(Optional.empty(),userService.update((String) null, "", "", null));
        assertEquals(Optional.empty(),userService.update("", "", "gfb", ""));
        assertEquals(Optional.empty(),userService.update("login", null, "gfb", ""));
        assertEquals(Optional.empty(),userService.update("login", "123", null, null));
        assertEquals(Optional.empty(),userService.update("login", "123", "faezf", null));
        when(userRepository.update(Mockito.any()))
                .thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.update("login", "123", "faezf", "dsd"));
    }

    @Test
    void updateById() {
        assertEquals(Optional.empty(),userService.update((Long) null, "", "", null));
        assertEquals(Optional.empty(),userService.update(1L, null, "gfb", ""));
        assertEquals(Optional.empty(),userService.update(1L, "123", null, null));
        assertEquals(Optional.empty(),userService.update(1L, "123", "faezf", null));
        when(userRepository.update(Mockito.any())).thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.update(1L, "123", "faezf", "dsd"));
    }

    @Test
    void updatePasswordByLogin() {
        assertEquals(Optional.empty(),userService.updatePasswordByLogin(null, ""));
        assertEquals(Optional.empty(),userService.updatePasswordByLogin("", null));
        assertEquals(Optional.empty(),userService.updatePasswordByLogin("login",  null));
        when(userRepository.updatePassword(Mockito.any())).thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.updatePasswordByLogin("login", "123"));
    }

    @Test
    void updatePasswordById() {
        assertEquals(Optional.empty(),userService.updatePasswordById(null, ""));
        assertEquals(Optional.empty(),userService.updatePasswordById(1L,  null));
        when(userRepository.updatePassword(Mockito.any())).thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.updatePasswordById(1L, "123"));
    }


    @Test
    void removeByName() {
        assertEquals(Optional.empty(),userService.removeByName(null));
        assertEquals(Optional.empty(),userService.removeByName(""));
        when(userRepository.removeByName("user1")).thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.removeByName("user1"));
    }

    @Test
    void removeById()  {
        assertEquals(Optional.empty(),userService.removeById(null));
        when(userRepository.removeById(1L)).thenReturn(Optional.of(user));
        assertEquals(Optional.of(user),userService.removeById(1L));
    }

    @Test
    void findAll()  {
        assertEquals(Optional.empty(),userService.findAll());
    }


    @Test
    void saveUpload() throws IOException {
        when(userRepository.saveJSON("test")).thenReturn(0);
        assertEquals(0, userService.saveJSON("test"));
        when(userRepository.saveXML("test")).thenReturn(0);
        assertEquals(0, userService.saveXML("test"));
        when(userRepository.uploadJSON("test")).thenReturn(0);
        assertEquals(0, userService.uploadFromJSON("test"));
        when(userRepository.uploadXML("test")).thenReturn(0);
        assertEquals(0, userService.uploadFromXML("test"));


        when(userRepository.saveJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.saveJSON(USERS_FILE_NAME_JSON));
        when(userRepository.saveXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.saveXML(USERS_FILE_NAME_XML));
        when(userRepository.uploadJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.uploadFromJSON(USERS_FILE_NAME_JSON));
        when(userRepository.uploadXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> userService.uploadFromXML(USERS_FILE_NAME_XML));

    }

    @Test
    void addCommandToHistory()  {
        when(userRepository.addCommandToHistory(Mockito.any())).thenCallRealMethod();
        userRepository.history = new ArrayDeque<>();
        userService.setHistoryLimit(10);
        assertEquals(-1,userService.addCommandToHistory(null));
        assertEquals(0,userService.addCommandToHistory("test"));
        for (int i = 0; i<=userService.getHistoryLimit();i++){
            userService.addCommandToHistory("test"+i);
        }
        assertEquals(1,userService.addCommandToHistory("test"));
    }

}