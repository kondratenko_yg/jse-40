package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.repository.ProjectRepository;
import ru.kondratenko.tm.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService{

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private final TaskRepository taskRepository=TaskRepository.getInstance();

    private static ProjectTaskService instance = null;

    private ProjectTaskService() {
    }

    public static ProjectTaskService getInstance(){
        if (instance == null )  instance = new ProjectTaskService();
        return instance;
    }

    @Override
    public Optional<Task> removeTaskFromProject(final Long projectId, final Long taskId) {
        final Optional<Task> task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task.isEmpty()) return Optional.empty();
        task.get().setProjectId(null);
        return task;
    }

    @Override
    public Optional<Task> addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Optional<Project> project = projectRepository.findById(projectId);
        if (project.isEmpty()) return Optional.empty();
        final Optional<Task> task = taskRepository.findById(taskId);
        if (task.isEmpty()) return Optional.empty();
        task.get().setProjectId(projectId);
        return task;
    }

    @Override
    public Optional<List<Task>> findAllByProjectId(Long projectId) {
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public void clear() {
        projectRepository.clear();
        taskRepository.clear();
    }

}
