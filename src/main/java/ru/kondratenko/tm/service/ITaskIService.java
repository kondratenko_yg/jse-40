package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.TaskNotFoundException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ITaskIService extends IService<Task> {
    Optional<Task> create(final String name);
    Optional<Task> create(final String name, final String description);
    Optional<Task> create(final String name, final String description, final Long userId);
    Optional<Task> create(final String name, final String description, final Long userId, final LocalDateTime time);
    Optional<Task> update(final Long id, final String name, final String description, final Long userId) throws TaskNotFoundException;
    Optional<Task> update(final Long id, final String name, final Long userId) throws TaskNotFoundException;
    Optional<List<Task>> findAllByProjectId(final Long projectId);
    Optional<Task> findByProjectIdAndId(final Long projectId, final Long id);
    Optional<List<Task>> findAllByUserId(Long Id);
    Optional<List<Task>> findByName(final String name) throws TaskNotFoundException;
    Optional<List<Task>> removeByName(final String name) throws TaskNotFoundException;
}
