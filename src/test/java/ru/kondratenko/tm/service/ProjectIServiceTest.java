package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.repository.ProjectRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.PROJECTS_FILE_NAME_JSON;
import static ru.kondratenko.tm.constant.TerminalConst.PROJECTS_FILE_NAME_XML;

class ProjectIServiceTest {
    private ProjectService projectService;
    private ProjectRepository projectRepository;
    private final Project project =  new Project("project1","desc",1L);

    @BeforeEach
    void setUp() {
        projectRepository = Mockito.mock(ProjectRepository.class);
        projectService = ProjectService.getInstance();
        ReflectionTestUtils.setField(projectService,"projectRepository",projectRepository);
    }

    @Test
    void create() {
        assertEquals(Optional.empty(),projectService.create(null));
        assertEquals(Optional.empty(),projectService.create(""));
        assertEquals(Optional.empty(),projectService.create("", ""));
        assertEquals(Optional.empty(),projectService.create("", "",1L));
        when(projectRepository.create(Mockito.any())).thenReturn(Optional.of(project));
        assertEquals(projectService.create("project1").get().getName(),project.getName());
        assertEquals(projectService.create("project1","desc").get().getDescription(),
                project.getDescription());
        projectService.clear();
    }

    @Test
    void update() throws ProjectNotFoundException {
        assertEquals(Optional.empty(),projectService.update(null, "", "", null));
        assertEquals(Optional.empty(),projectService.update(1L, "", "gfb", 2L));
        assertEquals(Optional.empty(),projectService.update(3L, null, "", null));
        when(projectRepository.update(Mockito.any())).thenReturn(Optional.of(project));
        assertEquals(Optional.of(project).get().getName()
                ,projectService.update(project.getId(),"project1","gj",null).get().getName());
        when(projectRepository.update(Mockito.any())).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.update(1L,"dt","gj",null));
    }

    @Test
    void findByIndex() throws ProjectNotFoundException {
        when(projectRepository.findByIndex(-1)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.findByIndex(-1));
        when(projectRepository.findByIndex(0)).thenReturn(Optional.of(project));
        assertEquals(Optional.of(project),projectService.findByIndex(0));
    }

    @Test
    void findByName() throws ProjectNotFoundException {
        assertEquals(Optional.empty(),projectService.findByName(null));
        assertEquals(Optional.empty(),projectService.findByName(""));
        when(projectRepository.findByName("project2")).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.findByName("project2"));
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        when(projectRepository.findByName("project1")).thenReturn(Optional.of(projects));
        assertEquals(Optional.of(projects),projectService.findByName("project1"));
    }

    @Test
    void findById() throws ProjectNotFoundException {
        assertEquals(Optional.empty(),projectService.findById(null));
        when(projectRepository.findById(2L)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.findById(2L));
        when(projectRepository.findById(1L)).thenReturn(Optional.of(project));
        assertEquals(Optional.of(project),projectService.findById(1L));
    }

    @Test
    void removeByIndex() throws ProjectNotFoundException {
        when(projectRepository.removeByIndex(-1)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.removeByIndex(-1));
        when(projectRepository.removeByIndex(0)).thenReturn(Optional.of(project));
        assertEquals(Optional.of(project),projectService.removeByIndex(0));
    }

    @Test
    void removeByName() throws ProjectNotFoundException {
        assertEquals(Optional.empty(),projectService.removeByName(null));
        assertEquals(Optional.empty(),projectService.removeByName(""));
        when(projectRepository.removeByName("project2")).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.removeByName("project2"));
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        when(projectRepository.removeByName("project1")).thenReturn(Optional.of(projects));
        assertEquals(Optional.of(projects),projectService.removeByName("project1"));
    }

    @Test
    void removeById() throws ProjectNotFoundException {
        assertEquals(Optional.empty(),projectService.removeById(null));
        when(projectRepository.removeById(2L)).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> projectService.removeById(2L));
        when(projectRepository.removeById(1L)).thenReturn(Optional.of(project));
        assertEquals(Optional.of(project),projectService.removeById(1L));
    }

    @Test
    void findAll()  {
        assertEquals(Optional.empty(),projectService.findAll());
    }

    @Test
    void findAllByUserId()  {
        assertEquals(Optional.empty(),projectService.findAllByUserId(null));
        assertEquals(Optional.empty(),projectService.findAllByUserId(1L));
    }

    @Test
    void saveUpload() throws IOException {
        when(projectRepository.saveJSON("test")).thenReturn(0);
        assertEquals(0, projectService.saveJSON("test"));
        when(projectRepository.saveXML("test")).thenReturn(0);
        assertEquals(0, projectService.saveXML("test"));
        when(projectRepository.uploadJSON("test",Project.class)).thenReturn(0);
        assertEquals(0, projectService.uploadFromJSON("test"));
        when(projectRepository.uploadXML("test",Project.class)).thenReturn(0);
        assertEquals(0, projectService.uploadFromXML("test"));


        when(projectRepository.saveJSON(PROJECTS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.saveJSON(PROJECTS_FILE_NAME_JSON));
        when(projectRepository.saveXML(PROJECTS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.saveXML(PROJECTS_FILE_NAME_XML));
        when(projectRepository.uploadJSON(PROJECTS_FILE_NAME_JSON,Project.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.uploadFromJSON(PROJECTS_FILE_NAME_JSON));
        when(projectRepository.uploadXML(PROJECTS_FILE_NAME_XML,Project.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> projectService.uploadFromXML(PROJECTS_FILE_NAME_XML));

    }

}