package ru.kondratenko.tm.repository;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kondratenko.tm.entity.AbstractEntity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;


public abstract class AbstractRepository<E extends AbstractEntity> implements ITaskProjectRepository<E> {

    public CopyOnWriteArrayList<E> items = new CopyOnWriteArrayList<>();

    public ConcurrentHashMap<String, List<E>> itemsA = new ConcurrentHashMap<>();

    @Override
    public abstract String getName(E item);

    @Override
    public abstract Long getUserId(Optional<E>item);

    @Override
    public Optional<E> create(final E item) {
        items.add(item);
        addToMap(item);
        return Optional.of(item);
    }

    @Override
    public void addToMap(final E item) {
        List<E> itemsInMap = itemsA.get(getName(item));
        if (itemsInMap == null) itemsInMap = new ArrayList<>();
        itemsInMap.add(item);
        itemsA.put(getName(item), itemsInMap);
    }

    @Override
    public Optional<List<E>> findAll() {
        return Optional.of(items);
    }

    @Override
    public void clear() {
        items.clear();
        itemsA.clear();
    }

    @Override
    public int size(final Long userId){
        if (userId == null) {
            return findAll().orElse(new ArrayList<>()).size();
        }
        else {
            return findAllByUserId(userId).orElse(new ArrayList<>()).size();
        }
    }

    @Override
    public Optional<List<E>> findAllByUserId(final Long userId) {
        final List<E> result = new ArrayList<>();
        for (final E item : findAll().orElse(new ArrayList<>())) {
            final Long IdUser = getUserId(Optional.of(item));
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(item);
        }
        return Optional.of(result);
    }

    @Override
    public int saveJSON(String fileName) throws IOException {
        final File file = new File(fileName);
        final File fileA = new File("A" + fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.writeValue(file, items);
        objectMapper.writeValue(fileA, itemsA);
        return 0;
    }

    @Override
    public int saveXML(String fileName) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File("A" + fileName), itemsA);
        xmlMapper.writeValue(new File(fileName), items);
        return 0;
    }

    @Override
    public int uploadJSON(String fileName, Class<E> clazz) throws IOException {
        final File file = new File(fileName);
        final File fileA = new File("A" + fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        clear();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        JavaType type = typeFactory.constructCollectionType(List.class, clazz);
        JavaType typeString = typeFactory.constructType(String.class);
        MapType mapType = typeFactory.constructMapType(HashMap.class, typeString, type);
        items = objectMapper.readValue(file, type);
        itemsA = objectMapper.readValue(fileA, mapType);
        if (checkItemsOfCollections()) {
            itemsA.clear();
            for (E item : items) {
                addToMap(item);
            }
        }
        return 0;
    }

    @Override
    public int uploadXML(String fileName, Class<E> clazz) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        JavaType type = xmlMapper.getTypeFactory().constructCollectionType(List.class, clazz);
        JavaType typeString = xmlMapper.getTypeFactory().constructType(String.class);
        MapType mapType = xmlMapper.getTypeFactory().constructMapType(HashMap.class, typeString, type);
        clear();
        items = xmlMapper.readValue(new File(fileName), type);
        itemsA = xmlMapper.readValue(new File("A" + fileName), mapType);
        if (checkItemsOfCollections()) {
            itemsA.clear();
            for (E item : items) {
                addToMap(item);
            }
        }
        return  0;
    }

    private boolean checkItemsOfCollections() {
        for (E item : items) {
            if (itemsA.get(getName(item)) == null) {
                return true;
            }
            if (!itemsA.get(getName(item)).contains(item)) {
                return true;
            }
        }
        return false;
    }

}
