package ru.kondratenko.tm.observer.Publisher;

import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.observer.Listener.Listener;

import java.io.IOException;
import java.util.Scanner;

public interface Publisher {
    void addListener(Listener listener);
    void deleteListener(Listener listener);
    void notify(String command) throws NotFoundException, IOException;
    void start(Scanner scanner) throws NotFoundException, IOException;
    boolean checkCommand(String command);
}
