package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.repository.ProjectRepository;
import ru.kondratenko.tm.repository.TaskRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class ProjectTaskIServiceTest {
    private ProjectTaskService projectTaskService;
    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;
    private final Task task = new Task("task1");
    private final Project project = new Project("project1");

    @BeforeEach
    void setUp() {
        projectRepository = Mockito.mock(ProjectRepository.class);
        taskRepository = Mockito.mock(TaskRepository.class);
        projectTaskService = ProjectTaskService.getInstance();
        ReflectionTestUtils.setField(projectTaskService,"projectRepository",projectRepository);
        ReflectionTestUtils.setField(projectTaskService,"taskRepository",taskRepository);
    }

    @Test
    void removeTaskFromProject() {
        when(taskRepository.findByProjectIdAndId(1L,1L)).thenReturn(Optional.empty());
        assertEquals(Optional.empty(),projectTaskService.removeTaskFromProject(1L, 1L));
        when(taskRepository.findByProjectIdAndId(2L,2L)).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task),projectTaskService.removeTaskFromProject(2L,2L));
    }

    @Test
    void addTaskToProject() throws ProjectNotFoundException, TaskNotFoundException {
        when(projectRepository.findById(1L)).thenReturn(Optional.empty());
        assertEquals(Optional.empty(),projectTaskService.addTaskToProject(1L, 1L));

        when(projectRepository.findById(3L)).thenReturn(Optional.of(project));
        when(taskRepository.findById(3L)).thenReturn(Optional.empty());
        assertEquals(Optional.empty(),projectTaskService.addTaskToProject(3L, 3L));

        when(projectRepository.findById(2L)).thenReturn(Optional.of(project));
        when(taskRepository.findById(2L)).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task),projectTaskService.addTaskToProject(2L,2L));
        projectTaskService.clear();
    }

    @Test
    void findAllByProjectId()  {
        when(taskRepository.findAllByProjectId(project.getId())).thenReturn(Optional.empty());
        assertEquals(Optional.empty(),projectTaskService.findAllByProjectId(project.getId()));
    }

}