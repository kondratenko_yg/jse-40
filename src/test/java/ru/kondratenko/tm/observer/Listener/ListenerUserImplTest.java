package ru.kondratenko.tm.observer.Listener;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.*;
import static ru.kondratenko.tm.util.HashUtil.hashMD5;
import static ru.kondratenko.tm.util.RequestParser.ACTION_SPLITTER;
import static ru.kondratenko.tm.util.RequestParser.PARAM_SPLITTER;

class ListenerUserImplTest {
    private ListenerUserImpl listenerUser;
    private final UserService userService= Mockito.mock(UserService.class);
    private final User user =  new User("user1",hashMD5("123"), Role.ADMIN);
    private final List<User> users = new ArrayList<>();
    @BeforeEach
    void setUp() {
        when(userService.getCurrentUser()).thenReturn(user);
        users.add(user);
        listenerUser = new ListenerUserImpl();
        when(userService.findAll()).thenReturn(Optional.of(users));
        when(userService.findByName("user1")).thenReturn(Optional.of(user));
        when(userService.findByName("user2")).thenReturn(Optional.empty());
        when(userService.findById(1L)).thenReturn(Optional.of(user));
        when(userService.findById(2L)).thenReturn(Optional.empty());
        when(userService.checkPassword(Mockito.any(),Mockito.any())).thenCallRealMethod();        ReflectionTestUtils.setField(listenerUser,"userService",userService);
    }

    @Test
    void updateInvalidCommand() throws IOException, NotFoundException {
        assertEquals(-1,listenerUser.update("ergg"));
    }

    @Test
    void displayHistory() throws IOException, NotFoundException {
        assertEquals(-1,listenerUser.update(HISTORY));
        Deque<String> history = new ArrayDeque<>();
        history.add("test");
        when(userService.getHistory()).thenReturn(history);
        assertEquals(0,listenerUser.update(HISTORY));
    }

    @Test
    void displayVersion() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(VERSION));
    }

    @Test
    void displayAbout() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(ABOUT));
    }

    @Test
    void displayHelp() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(HELP));
    }

    @Test
    void displayExit() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(EXIT));
    }

    @Test
    void registry() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(LOG_ON+" user1 "+ "123"));
        assertEquals(-1,listenerUser.update(LOG_ON+"user1"+"1234"));
        List<String> parameters = new ArrayList<>(Arrays.asList("user2", "123"));
        assertEquals(-1,listenerUser.registry(parameters));
    }

    @Test
    void logOff() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(LOG_OFF));
    }

    @Test
    void displayUserInfo() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(USER_INFO));
    }
    
    @Test
    void updatePassword() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(USER_UPDATE_PASSWORD+" 123 "+"1234"));
        assertEquals(-1,listenerUser.update(USER_UPDATE_PASSWORD+" 1234 "+"1234"));
        when(userService.getCurrentUser()).thenReturn(null);
        assertEquals(-1,listenerUser.update(USER_UPDATE_PASSWORD+" 1234 "+"1234"));
    }

    @Test
    void updatePasswordByName() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(USER_PASSWORD_UPDATE_BY_LOGIN+" user1 "+"121234"));
        assertEquals(-1,listenerUser.update(USER_PASSWORD_UPDATE_BY_LOGIN+" user2 "+"123234"));
    }

    @Test
    void createUser() throws IOException, NotFoundException {
        String data ="login"+PARAM_SPLITTER+"1234"+PARAM_SPLITTER+"firstname "+PARAM_SPLITTER+"lastname";
        when(userService.create("login","1234","firstname","lastname", Role.USER)).thenReturn(Optional.empty());
        assertEquals(-1,listenerUser.update(USER_CREATE+ACTION_SPLITTER+data));

        String dataOK ="login1"+PARAM_SPLITTER+"1234"+PARAM_SPLITTER+"firstname"+PARAM_SPLITTER+"lastname";
        when(userService.create("login1","1234","firstname","lastname", Role.USER)).thenReturn(Optional.of(user));

        assertEquals(0,listenerUser.update(USER_CREATE+ACTION_SPLITTER+dataOK));

    }

    @Test
    void clearUser() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(USER_CLEAR));
    }

    @Test
    void listUser() throws IOException, NotFoundException {
        assertEquals(0,listenerUser.update(USER_LIST));
        users.clear();
        assertEquals(-1,listenerUser.list());
        when(userService.getCurrentUser()).thenReturn(null);
        assertEquals(-1,listenerUser.update(USER_LIST));
    }

    @Test
    void removeUserByName()  throws IOException, NotFoundException {
        String dataText1 = "user1";
        when(userService.removeByName("user1")).thenReturn(Optional.of(user));
        assertEquals(0,listenerUser.update(USER_REMOVE_BY_LOGIN+ACTION_SPLITTER+dataText1));

        String dataText2 = "user2";
        when(userService.removeByName("user1")).thenReturn(Optional.empty());
        assertEquals(-1,listenerUser.update(USER_REMOVE_BY_LOGIN+ACTION_SPLITTER+dataText2));
        
    }

    @Test
    void saveXML() throws IOException {
        assertEquals(-1,listenerUser.saveXML(null));
        assertEquals(-1,listenerUser.saveXML(""));
        assertEquals(0,listenerUser.saveXML("test"));
        when(userService.saveXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_TO_FILE_XML));
    }

    @Test
    void saveJSON() throws IOException {
        assertEquals(-1,listenerUser.saveJSON(null));
        assertEquals(-1,listenerUser.saveJSON(""));
        assertEquals(0,listenerUser.saveJSON("test"));
        when(userService.saveJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_TO_FILE_JSON));
    }

    @Test
    void uploadFromXML() throws IOException {
        assertEquals(-1,listenerUser.uploadFromXML(null));
        assertEquals(-1,listenerUser.uploadFromXML(""));
        assertEquals(0,listenerUser.uploadFromXML("test"));
        when(userService.uploadFromXML(USERS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_FROM_FILE_XML));
    }

    @Test
    void uploadFromJSON() throws IOException {
        assertEquals(-1,listenerUser.uploadFromJSON(null));
        assertEquals(-1,listenerUser.uploadFromJSON(""));
        assertEquals(0,listenerUser.uploadFromJSON("test"));
        when(userService.uploadFromJSON(USERS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerUser.update(USERS_FROM_FILE_JSON));
    }

}