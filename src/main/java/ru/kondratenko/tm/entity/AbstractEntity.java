package ru.kondratenko.tm.entity;


import java.io.Serializable;

public abstract class AbstractEntity implements Serializable {
    Long id;

    String name;

    public AbstractEntity() {
    }

    public AbstractEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
