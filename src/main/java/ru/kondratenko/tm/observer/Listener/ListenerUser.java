package ru.kondratenko.tm.observer.Listener;

import java.util.List;

public interface ListenerUser extends Listener {
    int updatePasswordByLogin(List<String> parameters);
    int registry(List<String> parameters);
    int logOff();
    int updatePassword(List<String> parameters);
    int displayAbout();
    int displayVersion();
    int displayHelp();
    int displayExit();
    int displayHistory();
    int displayUserInfo();
}
