package ru.kondratenko.tm.observer.Listener;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.TaskService;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.*;
import static ru.kondratenko.tm.util.RequestParser.ACTION_SPLITTER;
import static ru.kondratenko.tm.util.RequestParser.PARAM_SPLITTER;

class ListenerTaskImplTest {
    private final TaskService taskService = Mockito.mock(TaskService.class);
    private final UserService userService= Mockito.mock(UserService.class);
    private final ProjectTaskService projectTaskService = Mockito.mock(ProjectTaskService.class);
    private ListenerTaskImpl listenerTaskImpl;
    private Task task;
    private List<Task> tasks;

    @BeforeEach
    void setUp() throws TaskNotFoundException {
        User user = new User("Test","123", Role.USER);
        when(userService.getCurrentUser()).thenReturn(user);
        tasks = new ArrayList<>();
        task = new Task("task1","desc",user.getId());
        tasks.add(task);

        when(taskService.findById(1L)).thenReturn(Optional.ofNullable(task));
        when(taskService.findByIndex(0)).thenReturn(Optional.ofNullable(task));
        when(taskService.findByName("task1")).thenReturn(Optional.of(tasks));

        when(taskService.findAllByUserId(user.getId())).thenReturn(Optional.of(tasks));
        when(taskService.findAllByProjectId(1L)).thenReturn(Optional.of(tasks));

        when(taskService.findById(2L)).thenThrow(TaskNotFoundException.class);
        when(taskService.findByIndex(1)).thenThrow(TaskNotFoundException.class);
        when(taskService.findByName("task2")).thenThrow(TaskNotFoundException.class);

        when(taskService.findById(3L)).thenReturn(Optional.empty());
        when(taskService.findByIndex(2)).thenReturn(Optional.empty());
        when(taskService.findByName("task3")).thenReturn(Optional.empty());
        when(taskService.findByIndex(-1)).thenReturn(Optional.empty());

        listenerTaskImpl = new ListenerTaskImpl();
        ReflectionTestUtils.setField(listenerTaskImpl,"taskService",taskService);
        ReflectionTestUtils.setField(listenerTaskImpl,"userService",userService);
        ReflectionTestUtils.setField(listenerTaskImpl,"projectTaskService",projectTaskService);
    }

    @Test
    void updateInvalidCommand() throws IOException, TaskNotFoundException, ProjectNotFoundException {
        assertEquals(-1,listenerTaskImpl.update("ergg"));
    }

    @Test
    void createTaskName() throws ProjectNotFoundException, IOException, TaskNotFoundException {
        assertEquals(-1,listenerTaskImpl.update(TASK_CREATE+" 1task"));
        List<String> parameters = new ArrayList<>(Arrays.asList("task","description"));
        assertEquals(0,listenerTaskImpl.create(parameters));
        
    }
    
    @Test
    void clear() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        assertEquals(0,listenerTaskImpl.update(TASK_CLEAR));
        when(taskService.removeById(task.getId())).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> listenerTaskImpl.clear());
        when(userService.getCurrentUser()).thenReturn(null);
        assertEquals(0,listenerTaskImpl.update(TASK_CLEAR));
    }

    @Test
    void list() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        assertEquals(0,listenerTaskImpl.update(TASK_LIST));
        tasks.clear();
        assertEquals(-1,listenerTaskImpl.list());
        when(userService.getCurrentUser()).thenReturn(null);
        assertEquals(-1,listenerTaskImpl.update(TASK_LIST));
    }

    @Test
    void viewTaskById() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        assertEquals(0,listenerTaskImpl.update(TASK_VIEW_BY_ID+" 1"));
        
        List<String> parameters = new ArrayList<>(Collections.singletonList("2"));
        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.viewById(parameters));
        
    }
    
    @Test
    void updateTaskByIndex() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        List<String> parameters = new ArrayList<>(Collections.singletonList("sd"));
        assertEquals(-1,listenerTaskImpl.updateByIndex(parameters));

        parameters.clear();
        parameters.add("-3");
        assertEquals(-1,listenerTaskImpl.updateByIndex(parameters));
        
        assertEquals(0,listenerTaskImpl.update(TASK_UPDATE_BY_INDEX+" 1 newname newd"));

        parameters.clear();
        parameters.add("2");parameters.add("newname");parameters.add("newd");
        assertThrows(TaskNotFoundException.class,()->
                listenerTaskImpl.updateByIndex(parameters));

        assertEquals(-1,listenerTaskImpl.update(TASK_UPDATE_BY_INDEX+"1 1newname newd"));
        
    }
            
    @Test
    void removeTaskByName() throws TaskNotFoundException, IOException, ProjectNotFoundException {
        List<String> parameters = new ArrayList<>(Collections.singletonList("task1"));
        when(taskService.removeByName("task1")).thenReturn(Optional.of(tasks));
        assertEquals(0,listenerTaskImpl.removeByName(parameters));

        when(taskService.removeByName("task2")).thenThrow(TaskNotFoundException.class);
        parameters.clear();
        parameters.add("task2");
        assertThrows(TaskNotFoundException.class,() ->
                listenerTaskImpl.removeByName(parameters));

        parameters.clear();
        parameters.add("task3");
        when(taskService.removeByName("task3")).thenReturn(Optional.empty());
        assertEquals(-1,listenerTaskImpl.update(TASK_REMOVE_BY_NAME));
        
    }

    @Test
    void saveXML() throws IOException {
        assertEquals(-1,listenerTaskImpl.saveXML(null));
        assertEquals(-1,listenerTaskImpl.saveXML(""));
        assertEquals(0,listenerTaskImpl.saveXML("test"));
        when(taskService.saveXML(TASKS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_TO_FILE_XML));
    }

    @Test
    void saveJSON() throws IOException {
        assertEquals(-1,listenerTaskImpl.saveJSON(null));
        assertEquals(-1,listenerTaskImpl.saveJSON(""));
        assertEquals(0,listenerTaskImpl.saveJSON("test"));
        when(taskService.saveJSON(TASKS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_TO_FILE_JSON));
    }

    @Test
    void uploadFromXML() throws IOException {
        assertEquals(-1,listenerTaskImpl.uploadFromXML(null));
        assertEquals(-1,listenerTaskImpl.uploadFromXML(""));
        assertEquals(0,listenerTaskImpl.uploadFromXML("test"));
        when(taskService.uploadFromXML(TASKS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_FROM_FILE_XML));
    }

    @Test
    void uploadFromJSON() throws IOException {
        assertEquals(-1,listenerTaskImpl.uploadFromJSON(null));
        assertEquals(-1,listenerTaskImpl.uploadFromJSON(""));
        assertEquals(0,listenerTaskImpl.uploadFromJSON("test"));
        when(taskService.uploadFromJSON(TASKS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerTaskImpl.update(TASKS_FROM_FILE_JSON));
    }

    @Test
    void listTaskByProjectId() throws IOException, ProjectNotFoundException, TaskNotFoundException {
        assertEquals(0,listenerTaskImpl.update(TASK_LIST_BY_PROJECT_ID+ " 1"));
        tasks.clear();
        List<String> parameters = new ArrayList<>(Collections.singletonList("1"));
        assertEquals(-1,listenerTaskImpl.listTaskByProjectId(parameters));
        
    }

    @Test
    void removeTaskFromProjectByIds() throws IOException, ProjectNotFoundException, TaskNotFoundException {
        long projectId = 1L;
        long taskId = 1L;
        assertEquals(0, listenerTaskImpl.update(
                TASK_REMOVE_FROM_PROJECT_BY_IDS+ACTION_SPLITTER+projectId+PARAM_SPLITTER+taskId));
        List<String> parameters = new ArrayList<>(Arrays.asList("1L","1L"));
        assertEquals(0,listenerTaskImpl.removeTaskFromProjectByIds(parameters));
        
    }

    @Test
    void addTaskToProjectByIds() throws IOException, ProjectNotFoundException, TaskNotFoundException {
        assertEquals(0,listenerTaskImpl.update(TASK_ADD_TO_PROJECT_BY_IDS+ " 1 1"));

        List<String> parameters = new ArrayList<>(Arrays.asList("1","1"));
        assertEquals(0,listenerTaskImpl.addTaskToProjectByIds(parameters));

    }


}