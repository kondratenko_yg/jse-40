package ru.kondratenko.tm.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.TaskNotFoundException;
import ru.kondratenko.tm.repository.TaskRepository;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.TASKS_FILE_NAME_JSON;
import static ru.kondratenko.tm.constant.TerminalConst.TASKS_FILE_NAME_XML;
import static ru.kondratenko.tm.util.HashUtil.hashMD5;

class TaskIServiceTest {
    private TaskService taskService;
    private UserService userService;
    private TaskRepository taskRepository;
    private Task task;

    @BeforeEach
    void setUp() {
        taskRepository = Mockito.mock(TaskRepository.class);
        userService = Mockito.mock(UserService.class);
        User user = new User("user1",hashMD5("123"), Role.ADMIN);
        userService.setCurrentUser(user);
        task =  new Task("task1","desc",user.getId(),1L);
        taskService = TaskService.getInstance();
        ReflectionTestUtils.setField(taskService,"taskRepository",taskRepository);
        ReflectionTestUtils.setField(taskService,"userService",userService);

    }

    @Test
    void create() throws TaskNotFoundException {
        assertEquals(Optional.empty(),taskService.create(null));
        assertEquals(Optional.empty(),taskService.create(""));
        assertEquals(Optional.empty(),taskService.create("", ""));
        assertEquals(Optional.empty(),taskService.create("", "",1L));
        when(taskRepository.create(Mockito.any())).thenReturn(Optional.of(task));
        when(taskRepository.removeById(task.getId())).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task)
                ,taskService.create("task1","desc",1L, LocalDateTime.now().plusMinutes(1L)));
    }

    @Test
    void update() throws TaskNotFoundException {
        assertEquals(Optional.empty(),taskService.update(null, "", "", null));
        assertEquals(Optional.empty(),taskService.update(1L, "", "gfb", 2L));
        assertEquals(Optional.empty(),taskService.update(3L, null, "", null));
        when(taskRepository.update(Mockito.any())).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task),taskService.update(1L,"dt","gj",null));
        when(taskRepository.update(Mockito.any())).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.update(1L,"dt","gj",null));
    }

    @Test
    void findByIndex() throws TaskNotFoundException {
        when(taskRepository.findByIndex(-1)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.findByIndex(-1));
        when(taskRepository.findByIndex(0)).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task),taskService.findByIndex(0));
    }

    @Test
    void findByName() throws TaskNotFoundException {
        assertEquals(Optional.empty(),taskService.findByName(null));
        assertEquals(Optional.empty(),taskService.findByName(""));
        when(taskRepository.findByName("task2")).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.findByName("task2"));
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        when(taskRepository.findByName("task1")).thenReturn(Optional.of(tasks));
        assertEquals(Optional.of(tasks),taskService.findByName("task1"));
    }

    @Test
    void findById() throws TaskNotFoundException {
        assertEquals(Optional.empty(),taskService.findById(null));
        when(taskRepository.findById(2L)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.findById(2L));
        when(taskRepository.findById(1L)).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task),taskService.findById(1L));
    }

    @Test
    void removeByIndex() throws TaskNotFoundException {
        when(taskRepository.removeByIndex(-1)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.removeByIndex(-1));
        when(taskRepository.removeByIndex(0)).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task),taskService.removeByIndex(0));
    }

    @Test
    void removeByName() throws TaskNotFoundException {
        assertEquals(Optional.empty(),taskService.removeByName(null));
        assertEquals(Optional.empty(),taskService.removeByName(""));
        when(taskRepository.removeByName("task2")).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.removeByName("task2"));
        List<Task> tasks = new ArrayList<>();
        tasks.add(task);
        when(taskRepository.removeByName("task1")).thenReturn(Optional.of(tasks));
        assertEquals(Optional.of(tasks),taskService.removeByName("task1"));
    }

    @Test
    void removeById() throws TaskNotFoundException {
        assertEquals(Optional.empty(),taskService.removeById(null));
        when(taskRepository.removeById(2L)).thenThrow(TaskNotFoundException.class);
        assertThrows(TaskNotFoundException.class,() -> taskService.removeById(2L));
        when(taskRepository.removeById(1L)).thenReturn(Optional.of(task));
        assertEquals(Optional.of(task),taskService.removeById(1L));
    }

    @Test
    void findAll()  {
        assertEquals(Optional.empty(),taskService.findAll());
    }

    @Test
    void findAllByUserId()  {
        assertEquals(Optional.empty(),taskService.findAllByUserId(null));
        assertEquals(Optional.empty(),taskService.findAllByUserId(1L));
    }

    @Test
    void saveUpload() throws IOException {
        when(taskRepository.saveJSON("test")).thenReturn(0);
        assertEquals(0, taskService.saveJSON("test"));
        when(taskRepository.saveXML("test")).thenReturn(0);
        assertEquals(0, taskService.saveXML("test"));
        when(taskRepository.uploadJSON("test",Task.class)).thenReturn(0);
        assertEquals(0, taskService.uploadFromJSON("test"));
        when(taskRepository.uploadXML("test",Task.class)).thenReturn(0);
        assertEquals(0, taskService.uploadFromXML("test"));


        when(taskRepository.saveJSON(TASKS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.saveJSON(TASKS_FILE_NAME_JSON));
        when(taskRepository.saveXML(TASKS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.saveXML(TASKS_FILE_NAME_XML));
        when(taskRepository.uploadJSON(TASKS_FILE_NAME_JSON,Task.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.uploadFromJSON(TASKS_FILE_NAME_JSON));
        when(taskRepository.uploadXML(TASKS_FILE_NAME_XML,Task.class)).thenThrow(IOException.class);
        assertThrows(IOException.class, () -> taskService.uploadFromXML(TASKS_FILE_NAME_XML));

    }

}