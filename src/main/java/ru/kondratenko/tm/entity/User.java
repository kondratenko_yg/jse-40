package ru.kondratenko.tm.entity;

import lombok.Builder;
import lombok.Data;
import ru.kondratenko.tm.enumerated.Role;

@Data
@Builder
public class User extends AbstractEntity {
    private Long id = System.nanoTime();

    private String name = "";

    private String password;

    private String firstName = "";

    private String lastName = "";

    private Role role = Role.USER;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, Role role) {
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public User(String name, String password, String firstName, String lastName, Role role) {
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public User(Long id, String name, String password, String firstName, String lastName, Role role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
