package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.UserNotFoundException;

import java.util.Deque;
import java.util.Optional;

public interface IUserIService extends IService<User> {
    Optional<User> create(
            String login, String password,
            String firstName, String lastName, Role role);
    boolean checkPassword(final Optional<User> user, final String password);
    Optional<User> update(String login, String password, String firstName, String lastName);
    Optional<User> update(Long id, String password, String firstName, String lastName);
    Optional<User> updatePasswordByLogin(String login, String password);
    Optional<User> updatePasswordById(Long id, String password);
    User getCurrentUser();
    Deque<String> getHistory();
    void setCurrentUser(User user);
    int addCommandToHistory(String command);
    int getHistoryLimit();
    void setHistoryLimit(int hisLimit);
    Optional<User> findByName(final String name) throws UserNotFoundException;
    Optional<User> removeByName(final String name) throws UserNotFoundException;
}
