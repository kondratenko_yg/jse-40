package ru.kondratenko.tm.observer.Listener;

import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;

import java.util.List;

public interface ListenerTask extends Listener  {
    int listTaskByProjectId(List<String> parameters);
    int addTaskToProjectByIds(List<String> parameters) throws ProjectNotFoundException, TaskNotFoundException;
    int removeTaskFromProjectByIds(List<String> parameters);
}
