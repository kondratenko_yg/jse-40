package ru.kondratenko.tm.observer.Listener;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.UserService;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static ru.kondratenko.tm.constant.TerminalConst.*;

class ListenerProjectImplTest {
    private final ProjectService projectService = Mockito.mock(ProjectService.class);
    private final ProjectTaskService projectTaskService = Mockito.mock(ProjectTaskService.class);
    private final UserService userService= Mockito.mock(UserService.class);
    private ListenerProjectImpl listenerProjectImpl;
    private Project project;
    private List<Project> projects;


    @BeforeEach
    void setUp() throws ProjectNotFoundException {
        User user = new User("Test","123", Role.USER);
        when(userService.getCurrentUser()).thenReturn(user);
        projects = new ArrayList<>();
        project = new Project("project1","desc",user.getId());
        projects.add(project);
        final List<Task> tasks = new ArrayList<>();
        Task task = new Task("task1");
        tasks.add(task);

        when(projectService.findById(1L)).thenReturn(Optional.ofNullable(project));
        when(projectService.findByIndex(0)).thenReturn(Optional.ofNullable(project));
        when(projectService.findByName("project1")).thenReturn(Optional.of(projects));

        when(projectService.findAllByUserId(user.getId())).thenReturn(Optional.ofNullable(projects));
        when(projectTaskService.findAllByProjectId(project.getId())).thenReturn(Optional.of(tasks));

        when(projectService.findById(2L)).thenThrow(ProjectNotFoundException.class);
        when(projectService.findByIndex(1)).thenThrow(ProjectNotFoundException.class);
        when(projectService.findByName("project2")).thenThrow(ProjectNotFoundException.class);

        when(projectService.findById(3L)).thenReturn(Optional.empty());
        when(projectService.findByIndex(2)).thenReturn(Optional.empty());
        when(projectService.findByName("project3")).thenReturn(Optional.empty());
        when(projectService.findByIndex(-1)).thenReturn(Optional.empty());

        listenerProjectImpl = new ListenerProjectImpl();
        ReflectionTestUtils.setField(listenerProjectImpl,"projectService",projectService);
        ReflectionTestUtils.setField(listenerProjectImpl,"projectTaskService",projectTaskService);
        ReflectionTestUtils.setField(listenerProjectImpl,"userService",userService);
    }

    @Test
    void updateInvalidCommand() throws IOException, ProjectNotFoundException {
        assertEquals(-1,listenerProjectImpl.update("ergg"));
    }

    @Test
    void createProjectWrongProjectName() throws IOException, ProjectNotFoundException {
        assertEquals(-1,listenerProjectImpl.update(PROJECT_CREATE+" 1project"));

        List<String> parameters = new ArrayList<>(Arrays.asList("project","description"));
        assertEquals(0,listenerProjectImpl.create(parameters));
    }


    @Test
    void clear() throws ProjectNotFoundException, IOException {
        assertEquals(0,listenerProjectImpl.update(PROJECT_CLEAR));
        when(projectService.removeById(project.getId())).thenThrow(ProjectNotFoundException.class);
        assertThrows(ProjectNotFoundException.class,() -> listenerProjectImpl.clear());
        when(userService.getCurrentUser()).thenReturn(null);
        assertEquals(0,listenerProjectImpl.update(PROJECT_CLEAR));
    }

    @Test
    void list() throws ProjectNotFoundException, IOException {
        assertEquals(0,listenerProjectImpl.update(PROJECT_LIST));
        projects.clear();
        assertEquals(-1,listenerProjectImpl.list());
        when(userService.getCurrentUser()).thenReturn(null);
        assertEquals(-1,listenerProjectImpl.update(PROJECT_LIST));
    }
    

    @Test
    void viewProjectById() throws ProjectNotFoundException, IOException {
        assertEquals(0,listenerProjectImpl.update(PROJECT_VIEW_BY_ID+" 1"));
        List<String> parameters = new ArrayList<>(Collections.singletonList("2"));
        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.viewById(parameters));
    }
    
    @Test
    void updateProjectByIndex() throws ProjectNotFoundException, IOException {
        List<String> parameters = new ArrayList<>(Collections.singletonList("sd"));
        assertEquals(-1,listenerProjectImpl.updateByIndex(parameters));

        parameters.clear();
        parameters.add("-3");
        assertEquals(-1,listenerProjectImpl.updateByIndex(parameters));

        parameters.clear();
        parameters.add("1");parameters.add("newname");parameters.add("newd");
        assertEquals(0,listenerProjectImpl.updateByIndex(parameters));

        parameters.clear();
        parameters.add("2");parameters.add("newname");parameters.add("newd");
        assertThrows(ProjectNotFoundException.class,()->
                listenerProjectImpl.updateByIndex(parameters));

        parameters.clear();
        parameters.add("1");parameters.add("1newname");parameters.add("newd");
        assertEquals(-1,listenerProjectImpl.update(PROJECT_UPDATE_BY_INDEX));
        
    }
    
    @Test
    void removeProjectByName() throws ProjectNotFoundException, IOException {
        List<String> parameters = new ArrayList<>(Collections.singletonList("project1"));
        when(projectService.removeByName("project1")).thenReturn(Optional.of(projects));
        assertEquals(0,listenerProjectImpl.removeByName(parameters));

        when(projectService.removeByName("project2")).thenThrow(ProjectNotFoundException.class);
        parameters.clear();
        parameters.add("project2");
        assertThrows(ProjectNotFoundException.class,() ->
                listenerProjectImpl.removeByName(parameters));

        parameters.clear();
        parameters.add("project3");
        when(projectService.removeByName("project3")).thenReturn(Optional.empty());
        assertEquals(-1,listenerProjectImpl.update(PROJECT_REMOVE_BY_NAME));
        
    }

    @Test
    void saveXML() throws IOException {
        assertEquals(-1,listenerProjectImpl.saveXML(null));
        assertEquals(-1,listenerProjectImpl.saveXML(""));
        assertEquals(0,listenerProjectImpl.saveXML("test"));
        when(projectService.saveXML(PROJECTS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_TO_FILE_XML));
    }

    @Test
    void saveJSON() throws IOException {
        assertEquals(-1,listenerProjectImpl.saveJSON(null));
        assertEquals(-1,listenerProjectImpl.saveJSON(""));
        assertEquals(0,listenerProjectImpl.saveJSON("test"));
        when(projectService.saveJSON(PROJECTS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_TO_FILE_JSON));
    }

    @Test
    void uploadFromXML() throws IOException {
        assertEquals(-1,listenerProjectImpl.uploadFromXML(null));
        assertEquals(-1,listenerProjectImpl.uploadFromXML(""));
        assertEquals(0,listenerProjectImpl.uploadFromXML("test"));
        when(projectService.uploadFromXML(PROJECTS_FILE_NAME_XML)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_FROM_FILE_XML));
    }

    @Test
    void uploadFromJSON() throws IOException {
        assertEquals(-1,listenerProjectImpl.uploadFromJSON(null));
        assertEquals(-1,listenerProjectImpl.uploadFromJSON(""));
        assertEquals(0,listenerProjectImpl.uploadFromJSON("test"));
        when(projectService.uploadFromJSON(PROJECTS_FILE_NAME_JSON)).thenThrow(IOException.class);
        assertThrows(IOException.class,() -> listenerProjectImpl.update(PROJECTS_FROM_FILE_JSON));
    }

}