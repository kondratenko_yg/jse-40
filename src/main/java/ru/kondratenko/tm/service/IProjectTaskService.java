package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.exception.ProjectNotFoundException;
import ru.kondratenko.tm.exception.TaskNotFoundException;

import java.util.List;
import java.util.Optional;

public interface IProjectTaskService {
    Optional<Task> addTaskToProject(final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException;
    Optional<Task> removeTaskFromProject(final Long projectId, final Long taskId);
    void clear();
    Optional<List<Task>> findAllByProjectId(Long projectId);
}
